import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './main/login/login.component';
import { ShareModule } from './shared/share/share.module';
import { ConfigModule } from './main/config/config.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ShareModule,
    // ConfigModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
