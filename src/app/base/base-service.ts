import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

export  class BaseService {
    mocData = [
        {
            id:1,
            code:'code',
            name:'name'
        }
    ]
    constructor(
        public http:HttpClient
    ) {
        
    }

    callService<T>(action: string, data?: any[]): Observable<IapiResponse<T>> {
        const body = {
            apiRequest: {
                action: action
            }
            , data: data || []
        };
        return this.http.post<IapiResponse<T>>('', body)
    }


    mockHttp() :Observable<any>{
        return of(this.mocData)
    }
}

export interface IapiResponse<T>{
    apiResponse:{
        id:number,
        desc:string,
    },
    data:Array<T>
}