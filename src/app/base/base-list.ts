export class BaseList {
    confiNgxDataTable: ConfigNgxDataTable = {
        class:'material fullscreen',
        columnMode:'flex',
        headerHeight: 50,
        footerHeight: 50,
        rowHeight:'auto',
        width:50,
        sortable: true,
        canAutoResize: false,
        draggable: false,
        resizeable: false,
        headerCheckboxable: false,
        checkboxable: false,
        alignAction:'center',
    }
    constructor() {    
        
    }

}

export interface ConfigNgxDataTable {
    class: string
    columnMode: string
    headerHeight: number
    footerHeight: number
    rowHeight: string
    width: number
    sortable: boolean;
    canAutoResize: boolean
    draggable: boolean
    resizeable: boolean
    headerCheckboxable: boolean
    checkboxable: boolean
    alignAction: string
}