import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateComponent } from './rate/rate.component';
import { Routes, RouterModule } from '@angular/router';
const route:Routes = [{
  path:'',
  component:RateComponent
}]


@NgModule({
  declarations: [RateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class RateModule { }
