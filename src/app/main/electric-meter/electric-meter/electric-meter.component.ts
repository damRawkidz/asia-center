import { Component, OnInit } from '@angular/core';
import { EletricMeterService } from '../eletric-meter.service';
import { BaseList } from 'src/app/base/base-list';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-electric-meter',
  templateUrl: './electric-meter.component.html',
  styleUrls: ['./electric-meter.component.scss']
})
export class ElectricMeterComponent extends BaseList implements  OnInit {
  body
  constructor(
    public ElectricService:EletricMeterService
  ) {
    super();
  }

  ngOnInit(): void {
    this.ElectricService.mockHttp().pipe(
        tap(x => this.body = x)
      )
      .subscribe(x => {console.log(x)})
  }
  

}
