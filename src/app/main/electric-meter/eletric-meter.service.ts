import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/base/base-service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EletricMeterService extends BaseService {

  constructor(
    public http:HttpClient
  ) {
    super(http);
  }
}
