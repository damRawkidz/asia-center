import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectricMeterComponent } from './electric-meter/electric-meter.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ShareModule } from 'src/app/shared/share/share.module';
const routes:Routes = [{
    path:'',
    component:ElectricMeterComponent
}]

@NgModule({
  declarations: [
    ElectricMeterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ShareModule
  ],
  exports:[
    ElectricMeterComponent,
    
  ]
})
export class ElectricMeterModule { }
