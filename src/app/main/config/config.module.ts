import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigContainerComponent } from './config-container/config-container.component';
import { AppRoutingModule } from 'src/app/app-routing.module';



@NgModule({
  declarations: [
    ConfigContainerComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],

  exports: [
    ConfigContainerComponent
  ]
})
export class ConfigModule { }
