import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './main/login/login.component';
import { ConfigContainerComponent } from './main/config/config-container/config-container.component';
import { ElectricMeterComponent } from './main/electric-meter/electric-meter/electric-meter.component';

const routes: Routes = [
  {
    path:'',
    pathMatch:'full',
    redirectTo:'login'
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'config',
    component:ConfigContainerComponent,
    children:[
      {
        path:'',
        pathMatch:'full',
        redirectTo:'meter'
      },
      {
        path:'meter',
        loadChildren: () => import('./main/electric-meter/electric-meter.module').then(m => m.ElectricMeterModule)
      },
      {
        path:'rate',
        loadChildren: () => import('./main/rate/rate.module').then(m => m.RateModule)
      }
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
